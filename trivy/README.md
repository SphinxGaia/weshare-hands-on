# Trivy : Little Red Riding Hood - Weshare édition

## Some helper commands


Get Vulns inside your containers
```bash
kubectl -n $NAMESPACE get vulnerabilityreports -o wide
```

Get weak configuration about your Kube ressources
```bash
kubectl -n $NAMESPACE get configauditreports -o wide

```

## Links

[Quickstart Trivy operator](https://aquasecurity.github.io/trivy-operator/v0.8.0/operator/quick-start/)


## Back

[Next Step](../)
