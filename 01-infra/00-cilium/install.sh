#!/usr/bin/env bash

cilium install --version v1.13.0 --cluster-name goldielock-cnis-cilium --helm-set-string prometheus.enabled=true,operator.prometheus.enabled=true,hubble.enabled=true,hubble.metrics.enableOpenMetrics=true,hubble.metrics.enabled="{dns,drop,tcp,flow,port-distribution,icmp,httpV2:exemplars=true;labelsContext=source_ip\,source_namespace\,source_workload\,destination_ip\,destination_namespace\,destination_workload\,traffic_direction}"

kubectl create -f ../02-$mycni-monitoring/dashboard.yaml

kubectl apply -f ../02-$mycni-monitoring/monitoring.yaml

kubectl apply -f ../02-$mycni-monitoring/node-exporter.yaml

kubectl apply -f ../02-$mycni-monitoring/kube-state-metrics/examples/standard/


cilium hubble enable --ui

echo "cilium hubble ui"

echo "kubectl -n cilium-monitoring port-forward service/grafana --address 0.0.0.0 --address :: 3000:3000"