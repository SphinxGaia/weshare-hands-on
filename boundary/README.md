# Boundary : Little Red Riding Hood - Weshare édition

## TODO

### Connect to Boundary

```bash

export BOUNDARY_ADDR="http://ac6c7a66b9daf4986939b7e7dae09ff0-1723290738.eu-west-1.elb.amazonaws.com:9200"

export GROUP=<my-group>

boundary authenticate password -login-name $GROUP -auth-method-id ampw_IZQGurunLI
```

> Create ~/.boundary-token

### Test you kubernetes connection

```bash
aws-vault exec $GROUP -- boundary connect kube -target-id=ttcp_2tlcncAcO1 -token env://BOUNDARY_TOKEN -- get no
```

## Back

[Next Step](/)