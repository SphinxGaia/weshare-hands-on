# Préparation de votre poste utilisateur

> ########################################
> 
> Warning /!\ ceci est un lab, la configuration fournie n'est pas Prod-Ready /!\
> 
> ########################################

Pour ce labs, vous devez :
  [ ] Configurer votre shell
    [ ] Avoir votre shell configurer avec des tokens pour AWS et un rôle vous permettant de créer VPC et autres configurations nécessaire pour votre/cos clusters EKS  
  [ ] Installer des binaires
    [ ] Installer Terraform: `>= 0.13.1`
    [ ] Installer la CLI cilium : https://docs.cilium.io/en/v1.13/gettingstarted/k8s-install-default/#install-the-cilium-cli
    [ ] Installer HELM
  [ ] Configurer l'environnement de Terraform
    [ ] Mettre à jour les fichiers YAML en accord avec votre environnement `01-infra/aws/envs/...yaml`
    [ ] la partie CIDR doit être un /16 au risque de voir les configurations en place poser des soucis
  [ ] Niveau connexion internet
    [ ] ëtre dans la capacité de vous connecter sur des ports exotiques
  


```bash
alias cmd_prefix="aws-vault exec custom -- "

cmd_prefix aws sts get-caller-identity


export mycni="cilium"
export currentinstall=$(pwd)

cmd_prefix aws sts get-caller-identity

echo -e "### CREATING EKS CLUSTER ### \n"

cd $currentinstall/01-infra/aws-cni/aws

terraform workspace select $mycni || terraform init && terraform workspace new $mycni



echo -e "### CHECKING EKS CLUSTER ### \n"

cmd_prefix aws eks update-kubeconfig --region eu-west-1 --name goldielock-cnis-$mycni

kubectx arn:aws:eks:eu-west-1:955480398230:cluster/goldielock-cnis-$mycni

cmd_prefix k9s 

echo -e "### INSTALL $mycni CNI ### \n"

cd $currentinstall/01-infra/00-$mycni

cmd_prefix ./install.sh


echo -e "### INSTALL vcluster ### \n"

cd $currentinstall/01-infra/00-vcluster

cmd_prefix ./install.sh

echo -e "### CREATE NEW vcluster ### \n"

cd $currentinstall/01-infra/00-vcluster

cmd_prefix ./newcluster.sh my-cluster2
```

> Vcluster create virtual cluster