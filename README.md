# Hands-on : Little Red Riding Hood - Weshare édition

## Intro

### Configure aws-vault :

OPTIONNEL:

```bash
export AWS_VAULT_BACKEND="file"
```

Configure you aws-vault

```bash

export GROUP=<my-group>

aws-vault add $GROUP

``` 

### Connect to EKS

```bash
aws-vault exec $GROUP -- aws eks update-kubeconfig --region eu-west-1 --name red-riding-hood
```

## Linkerd

[Step Linkerd](linkerd/)

## Trivy

[Step Trivy](trivy/)

## Boundary

[Step Boundary](boundary/)